﻿var socket = new WebSocket("ws://localhost:8083");
socket.onopen = function() {
    //alert("Соединение установлено.");
    socket.send(JSON.stringify({"method": "login", "password": prompt("Input password","root")}));

};
var flag=false;
var delimetr="\\";
var answer;
var id;
socket.onmessage = function(event) {
    window.answer=JSON.parse(event.data);;
    if (answer["request_name"]=="login"){
        socket.send(JSON.stringify({"method": "login", "password": prompt(answer['error_message'],"root")}));
    }else if (answer['error']==true){
        alert(answer['error_message']);
        socket.send(JSON.stringify({"id":id,"method": "get_current_dir", "params": null}));
        mainApp.reset();
    }
    if (answer["request_name"]=="message"){
                    alert("Succes!");
                    id=answer['message']
                    socket.send(JSON.stringify({"id":id,"method": "get_current_dir", "params": null}));
                    socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
    }
    if (answer["id"]==id)
        {
            if (answer["request_name"]=="show_dir_content"){
                mainApp.loadStorage();
            }
            if ((answer["request_name"]=="read_file" ||  answer["request_name"]=="change_dir") && mainApp.read_type=="show"){
                mainApp.showNote();
                socket.send(JSON.stringify({"id":id,"method": "get_current_dir", "params": null}));
            }
            if (answer["request_name"]=="up"){
                mainApp.up();
                socket.send(JSON.stringify({"id":id,"method": "get_current_dir", "params": null}));
            }
            if (answer["request_name"]=="remove_file" ||  answer["request_name"]=="remove_dir"){
                mainApp.deleteItem();
            }
            if (answer["request_name"]=="create_file" ||  answer["request_name"]=="new_dir"){
                mainApp.addNote();
            }
            if (answer["request_name"]=="read_file" && mainApp.read_type=="edit"){
                mainApp.editItem();
            }
            if (answer["request_name"]=="edit_file" ||  answer["request_name"]=="new_name"){
                mainApp.saveItem();
            }
            if (answer["request_name"]=="get_current_dir")
                mainApp.path=answer["result"]+delimetr;
        }
};
    
    var mainApp = new Vue({
                el: '#mainApp',
                data: {
                    path:null,
                    isFile:false,
                    old_name: null,
                	name: "",
                	content: "",
                    lst: [],
                    isVisible: false,
                    showNote_flag:false,
                    isRequest:false,
                    read_type: "edit",
                },
                methods: {
                    addNote: function(){
                    	this.clear_queue();
                        if (this.isRequest==false){
                            obj = {
                                'name': 'new_name',
                                'type':true,
                            };
                            name=prompt("Имя", "new_name");
                            if (name!=null && name!="null"){
                                if (name.substr(name.length - 4)==".txt")
                                {
                                    socket.send(JSON.stringify({"id":id,"method": "create_file", "params": {"name":this.path+name}}));
                                    obj['type']=false;
                                }
                                else
                                {
                                    socket.send(JSON.stringify({"id":id,"method": "new_dir", "params": {"name":this.path+name}}));
                                    obj['type']=true;
                                }
                                this.isRequest=true;
                            }
                            
                        }else{
                            socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                            this.isRequest=false;
                        }
                    },
                    clear_queue:function(){
                    	socket.send(JSON.stringify({"id":id,"method": "clear_queue", "params": null}));
                    },
                    up: function(){
                    	this.clear_queue();
                        if (this.isRequest==false){
                            socket.send(JSON.stringify({"id":id,"method": "up", "params": null}));
                            this.isRequest=true;
                        }else{
                            socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                            this.isRequest=false;
                        }
                    },
                    reset: function(){
                    	this.clear_queue();
                        this.old_name=null;
                        this.isFile=false;
                        this.name= "";
                        this.content= "";
                        this.isVisible= false;
                        this.showNote_flag=false;
                        this.isRequest=false;
                        this.read_type= "edit";
                        socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                    },
                    loadStorage: function(){
                        this.lst=[];
                		file_names=answer['result'];
                        type="";
                        for (var i =0;i<file_names.length;i++){
                            if (file_names[i].substr(file_names[i].length - 4)==".txt") 
                                type=true;
                            else 
                                type=false;
                            var data = {
                                'name': file_names[i],
                                'type':type
                            }
                            this.lst.push(data);
                        }

                    },
                    deleteItem: function(item){
                    	this.clear_queue();
                        //alert("Вызывали?)");
                        if (this.isRequest==false){
                            if (item.name.substr(item.name.length - 4)==".txt")
                                socket.send(JSON.stringify({"id":id,"method": "remove_file", "params": {"name":this.path+item.name}}));
                            else
                                socket.send(JSON.stringify({"id":id,"method": "remove_dir", "params": {"name":this.path+item.name}}));
                            for (i = 0; i < this.lst.length; i++) {
                            	if (this.lst[i].name==item.name){
                            		this.lst.splice(i, 1);
                            	}
                            }
                            this.isRequest=true;
                        }else{
                            socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                            this.isRequest=false;
                        }
                    },
                    editItem: function(item){
                        this.isVisible = true;
                        this.showNote_flag=false;
                        if (this.isRequest==false ){
                        	this.clear_queue();
                            this.old_name=item.name;
                        	this.name=item.name;
                            this.isFile=false;
                            if (item.name.substr(item.name.length - 4)==".txt"){
                                this.isFile=true;
                                socket.send(JSON.stringify({"id":id,"method": "read_file", "params": {"path":this.path+item.name}}));
                            }
                            this.isRequest=true;
                            this.read_type="edit";
                        }else if (this.isRequest==true){
                            //socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                            this.content=answer['result'];
                            this.isRequest=false;
                        }
                        if (item){
                            if (item.name.substr(item.name.length - 4)!=".txt"){
                                this.name=item.name;
                                this.isFile=false;
                                this.isRequest=false;
                            }
                        }
               		},
               		showNote: function(item){
                        this.isVisible = false;
                        if (this.isRequest==false){
                        	this.clear_queue();
                            if (item.name.substr(item.name.length - 4)==".txt"){
                                socket.send(JSON.stringify({"id":id,"method": "read_file", "params": {"path":this.path+item.name}}));
                            }else{
                            	//alert("dasdasdsad ");
                                socket.send(JSON.stringify({"id":id,"method": "change_dir", "params": {"path":this.path+item.name}}));
                                this.name=item.name;
                            }
                            this.old_name=item.name;
                            this.isRequest=true;
                            this.read_type="show";
                        }else{
                            //console.log();
                            if (answer["request_name"]=="read_file"){
                                this.showNote_flag=true;
                                item_name=this.old_name;
                                this.name=item_name;
                                this.content=answer['result'];
                                this.isRequest=false;
                            }
                            if (answer["request_name"]=="change_dir"){
                                this.showNote_flag=false;
                                this.path=this.path+this.name+delimetr;
                                socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                                this.isRequest=false;
                            }
                        }
                        
               		},
               		saveItem: function(){
                        if (this.isRequest==false){
                            if (this.old_name.substr(this.old_name.length - 4)==".txt")
                                socket.send(JSON.stringify({"id":id,"method": "edit_file", 
                                    "params": {"old_name":this.path+this.old_name,
                                                "new_name":this.path+this.name,"content":this.content}}));
                            else  socket.send(JSON.stringify({"id":id,"method": "new_name", 
                                    "params": {"old_name":this.path+this.old_name,
                                                "new_name":this.path+this.name}}));
                        	this.name="";
                        	this.content="";
                        	this.isVisible = false;
                        	this.showNote_flag=false;
                            this.old_name=null;
                            this.isRequest=true;
                        }else{
                        	this.clear_queue();
                            socket.send(JSON.stringify({"id":id,"method": "show_dir_content", "params": null}));
                            this.isRequest=false;
                        }
               		}
                }
            });

    
