package com.company;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.*;

public class Main {

    public static void main(String[] args) {
        List<String> op_list= Arrays.asList("+","-","*","/");
        String example = " 1 1 23   12 1  2   * - + ";
        ArrayList<Integer> numbers=new ArrayList<Integer>();
        ArrayList<String> ops=new ArrayList<String>();
        for (String retval : example.split(" ")) {
            try {
                numbers.add(Integer.parseInt(retval));
            }catch (java.lang.NumberFormatException e){
                if (retval.length()>0)
                    if (op_list.contains(retval))
                        ops.add(retval);
                    else System.out.println("error");
            }
        }
        Collections.reverse(numbers);
        int k=0;
        float sum=numbers.get(0);
        numbers.remove(numbers.get(0));
        for (Integer i: numbers){
            System.out.println(ops.get(k));
            switch (ops.get(k)){
                case "+":
                    sum+=i;
                    break;
                case "-":
                    sum=i-sum;
                    break;
                case "*":
                    sum*=i;
                    break;
                case "/":
                    sum=i/sum;
                    break;
            }
            k++;
        }
        System.out.println(sum);
    }
}