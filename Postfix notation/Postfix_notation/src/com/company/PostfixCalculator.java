package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PostfixCalculator {
    private List<String> op_list= Arrays.asList("+","-","*","/");
    private ArrayList<Integer> numbers=new ArrayList<Integer>();
    private ArrayList<String> ops=new ArrayList<String>();
    private float result;

    @Override
    public String toString() {
        return String.valueOf(result);
    }
    public double calculate(String expression) throws Exception {
        numbers.clear();
        ops.clear();
        float sum = (float) 0.0;
        if (parse(expression)) {
            Collections.reverse(numbers);
            int k = 0;
            sum = numbers.get(0);
            numbers.remove(numbers.get(0));
            for (Integer i : numbers) {
                switch (ops.get(k)) {
                    case "+":
                        sum += i;
                        break;
                    case "-":
                        sum = i - sum;
                        break;
                    case "*":
                        sum *= i;
                        break;
                    case "/":
                        sum = i / sum;
                        break;
                }
                k++;
            }
        }
        this.result=sum;
        return sum;
    }
    private boolean parse(String expression) throws Exception {
        for (String retval : expression.split(" ")) {
            try {
                numbers.add(Integer.parseInt(retval));
            }catch (java.lang.NumberFormatException e){
                if (retval.length()>0)
                    if (op_list.contains(retval))
                        ops.add(retval);
                    else throw new Exception("Operator error");
            }
        }
        if ((numbers.size()-ops.size())!=1) throw new Exception("Quantity error");
        return true;
    }
    public static void main(String[] args) throws Exception {
        PostfixCalculator calc=new PostfixCalculator();
        System.out.println(calc.calculate("  23   12 1  2   * - + "));
    }
}
