package tests;

import com.company.PostfixCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class All_tests {
    @Test
    public void tests() throws Exception {
        PostfixCalculator calc=new PostfixCalculator();
        Assertions.assertEquals(calc.calculate("  23   12 1  2   * - + "),33);
        Assertions.assertEquals(calc.calculate("  7    2 3  * - "),1);
        try {
            calc.calculate(" 2 3 4  - + /");
        } catch(Exception e) {
            System.out.println("УСпех");
        }
    }
}
