# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 02:36:34 2018

@author: Диляра
"""

from websocket_server import WebsocketServer
import json
from FileManager import *
# Called for every client connecting (after handshake)
ids={}
fms={}
busy_paths={}
clients={}

def new_client(client, server):
    global ids,fms,busy_paths,clients
    ids[client['id']]=False
    clients[client['id']]=client
    busy_paths[client['id']]=[]
    print("BUSY",busy_paths)
    fms[client['id']]=FileManager(client['id'],busy_paths)
    print("New client connected and was given id %d" % client['id'])


def client_left(client, server):
    global ids,fms,busy_paths,clients
    del ids[client['id']]
    del fms[client['id']]
    del busy_paths[client['id']]
    del clients[client['id']]
    print("Client(%d) disconnected" % client['id'])

def message_received(client, server, message):
    global ids
    buf = json.loads(message)
    print("CECK ",ids[client['id']])
    if ids[client['id']]==False:
        try:
            if buf['password']:
                if buf['password']=="root":
                    ids[client['id']]=True
                    result=json.dumps({"request_name":"message", "error":False,
                                           "message":client['id']})
                else:
                    result=json.dumps({"request_name":"login", "error":True,
                                           "error_message":"Wrong!"})
                    print("Wrong")
                server.send_message(client,result)
        except:
            pass
    if ids[client['id']]==True:
        if "id" in buf:
            if buf["id"]==client['id']:
                fm=fms[buf["id"]]
                if buf['method'] == "exit":
                    server.sendMessage(client,b"bye",False)
                elif buf['method'] != "login":
                    try:
                        print("Request: ",buf)
                        if buf['params'] !=None:
                            result=fm.funcs[buf['method']](**buf['params'])
                        else:
                            result=fm.funcs[buf['method']]()
                        result={"request_name":buf['method'],"result":result, "error":False,
                                           "error_message":None}
                    except Exception as error:
                        result={"result":"", "error":True,
                                           "error_message":str(error)}
                        fm.change_dir("main_catalog")
                    result["id"]=client['id']
                    print("Answer: ",result)
                    server.send_message(client,json.dumps(result))
                    for i in ids:
                        if ids[i]==True:
                            print(i," called")
                            fm=fms[i]
                            result=fm.funcs['show_dir_content']()
                            result={"id":i,"request_name":"show_dir_content","result":result, "error":False,
                                                           "error_message":None}
                            server.send_message(clients[i],json.dumps(result))
                            result=fm.funcs['get_current_dir']()
                            result={"id":i,"request_name":"get_current_dir","result":result, "error":False,
                                                           "error_message":None}
                            server.send_message(clients[i],json.dumps(result))
                            

            
PORT=8083
server = WebsocketServer(PORT,host="localhost")
server.set_fn_new_client(new_client)
server.set_fn_client_left(client_left)
server.set_fn_message_received(message_received)
server.run_forever()