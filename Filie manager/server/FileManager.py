# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 14:34:50 2018

@author: Диляра
"""

import os,shutil
import json
global delimetr

delimetr="\ "
delimetr=delimetr[0]
class FileManager:
    def __init__(self,name,busy_paths):
        self.name=name
        self.main_dir=r"C:\My_projects\theory-and-practice-of-programming-languages\Filie manager\server\main_catalog"
        self.cur_dir="main_catalog"
        print("\n",self.main_dir,"\n")
        self.busy_paths=busy_paths
        self.funcs={
               "get_current_dir":self.get_current_dir,
               "show_dir_content":self.show_dir_content,
               "change_dir":self.change_dir,
               "up":self.up,
               "read_file":self.read_file,
               "edit_file":self.edit_file,
               "remove_file":self.remove_file,
               "remove_dir":self.remove_dir,
               "new_name":self.new_name,
               "create_file":self.create_file,
               "new_dir":self.new_dir,
               "clear_queue":self.clear_queue
               }
       
    def get_current_dir(self):
        return self.cur_dir
    

    def read_file(self,path):
        path=self.path_adapter(path)
        if (path[-4:]==".txt"):
            if self.is_exist(path):
                for i in self.busy_paths:
                    for j in self.busy_paths[i]:
                        if path==j:
                            raise Exception("File is busy")
                print(self.busy_paths,self.name)
                self.busy_paths[self.name].append(path)
                f=open(path,"r")
                buff=f.read()
                f.close()
                return buff
            else:
                raise Exception("Path isn not exist")
        else:
                raise Exception("Not .txt file")
    def edit_file(self,old_name,new_name,content):
        name1=self.path_adapter(old_name)
        name2=self.path_adapter(new_name)
        if self.is_exist(name1):
            if (name1[-4:]==".txt" and name2[-4:]==".txt"):
                self.remove_file(name1)
                f=open(name2,"w")
                f.write(content)
            else:
                raise Exception("Path doesn't not exists")
        else:
            raise Exception("No such file")

    
    def create_file(self,name):
        name=self.path_adapter(name)
        if not self.is_exist(name):
            if name[-4:]=='.txt':
                    file=open(name,"w")
            else:
                file=open(name+'.txt',"w")
        else:
            raise Exception("Such file already esists")
        file.close()

    def new_dir(self,name):
        if name.find(".")==-1:
            name=self.path_adapter(name)
            if not self.is_exist(name):
                try:
                    os.mkdir(name)
                except:
                    raise Exception("Directory was edited or removed")
            else:
                raise Exception("Such folder already esists")
        else:
            raise Exception("Invalid name")
        
    def remove_file(self,name):
        if name[0]!="C":
            name=self.path_adapter(name)
        os.remove(name)


    def remove_dir(self,name):
        name=self.path_adapter(name)
        shutil.rmtree(name)


    def new_name(self,old_name,new_name):
        if self.is_exist(self.path_adapter(old_name)):
            if not self.is_exist(self.path_adapter(new_name)):
                if new_name.find(".")==-1:
                    old_name=self.path_adapter(old_name)
                    new_name=self.path_adapter(new_name)
                    os.rename(old_name,new_name)
                else:
                    raise Exception("Invalid name")
            else:
                raise Exception("Such folder already exists")
        else:
            raise Exception("Path doesn't not exists")

    def clear_queue(self):
        self.busy_paths[self.name]=[]

    def show_dir_content(self):
        print(self.path_adapter(self.cur_dir))
        lst= os.listdir(self.path_adapter(self.cur_dir))
        result=[]
        for i in lst:
            if (i[-4:]==".txt"):
                result.append(i)
            else:
                if i.find(".")==-1:
                   result.append(i) 
        return result
    
    def path_adapter(self,path):
        return os.path.join(self.main_dir,delimetr.join(path.split(delimetr)[1:]))
    
    def is_exist(self,path):
        if os.path.exists(path):
            return True
        else:
            return False
        
    def up(self):
        self.cur_dir=delimetr.join(self.cur_dir.split(delimetr)[0:-1])
        if self.cur_dir=="":
            raise Exception("No permisson")
        return self.cur_dir
        
    def change_dir(self,path):
#        path=path.replace("/",delimetr)
        path=path.split(delimetr)
        if path[0]!="main_catalog":
            raise Exception("Invalid path")
        path=delimetr.join(path)
        if (self.is_exist(self.path_adapter(path))):
            self.cur_dir=path
            return (path)
        else:
            raise Exception("Invalid path")
            
if __name__ == "__main__":     
    f=FileManager()
#    f.change_dir("main_catalog\d1\d3")
#    print(f.get_current_dir())
#    f.change_dir("main_catalog")
    print(f.get_current_dir())
    print(f.show_dir_content())
    print(f.read_file("main_catalog\\asdsa21312.txt"))
#    f.up()
#    print(f.get_current_dir())
#    print(f.show_dir_content())